import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class BibDM{
    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }
    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
      Integer min = null;
      for(Integer elem:liste){
        if(min == null || min > elem){
          min = elem;
        }
      }
      return min;
    }
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        boolean res = true;
        for(T elem:liste){
          if(elem.compareTo(valeur) <= 0 ){
            res = false;
          }
        }
        return res;
    }
    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
      List<T> liste = new ArrayList<>();
      if(liste1.isEmpty()||liste2.isEmpty()){
        return null;}
      for(int i=0; i < liste1.size(); ++i){
        for(int j=0; j < liste2.size(); ++j){
          if(liste1.get(i).compareTo(liste2.get(j)) == 0){
            liste.add(liste1.get(i));
          }
        }
      }
      return liste;
    }
    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
      List<String> liste = new ArrayList<>();
      String mot = "";
      for(int i=0; i < texte.length(); ++i){
        char lettre = texte.charAt(i);
        if(lettre!=' '){
          mot += lettre;
        }
        else{
          if (mot!=""){
            liste.add(mot);
            mot = "";
          }
        }
      }
      if(mot != ""){
        liste.add(mot);
      }
      return liste;
    }
    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
      int max = 0;
      int cpt = 1;
      String motMax = null;
      List<String> liste = BibDM.decoupe(texte);
      for(String motAct:liste){
        for(String mot:liste){
          if(mot == motAct){
            cpt++;
          }
        }
        if(cpt > max){
          motMax = motAct;
        }
        cpt = 1;
      }
      return motMax;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
     public static boolean bienParenthesee(String chaine){
        int gauche = 0;
        int droite = 0;
        if(chaine.charAt(0) == ')'){
          return false;}
        for(int i=0; i<chaine.length();++i){
          if(chaine.charAt(chaine.length()-1)=='('){
            return false;}
          if(chaine.charAt(i)=='('){
            gauche += 1;
          }
          else if(chaine.charAt(i)==')'){
            droite += 1;
          }
        }
        if (gauche != droite){
          return false;}
        return true;
      }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
      if(chaine.charAt(0) == ')' || chaine.charAt(0) == ']'){
          return false;}
      for(int i=0; i<chaine.length();++i){
        if(chaine.charAt(i) == ']'){
          if(chaine.charAt(i-1) == '('){
            return false;
          }
        }
        if(chaine.charAt(i) == ')'){
          if(chaine.charAt(i-1) == '['){
            return false;
          }
        }
      }
      return true;
    }
    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      for(Integer entier:liste){
        if(entier == valeur){
          return true;
        }
      }
      return false;
    }

}
